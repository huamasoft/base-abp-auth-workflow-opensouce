﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyCompanyName.MyProject.ETag
{
    public class ETagCacheKey
    {
        public string Path { get; set; }

        public string Method { get; set; }

        public string Culture { get; set; }

        public int? TenantId { get; set; }

        public long? UserId { get; set; }


        public ETagCacheKey(string path, int? tenantId, long? userId, string culture)
        {
            Path = path.Trim().ToLower();
            TenantId = tenantId;
            UserId = userId;
            Culture = culture;
        }

        public override string ToString()
        {
            return $"{TenantId}:{UserId}@{Culture}-{Path}";
        }
    }
}
