import request from '@/utils/request'

export function getAllPermissions() {
  return request({
    url: '/api/services/app/Permission/GetAllPermissions',
    method: 'get'
  })
}
