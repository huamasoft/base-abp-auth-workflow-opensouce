import router from './router'
// import store from './store'
// import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'
import AppConsts from '@/vendor/abp/appconst'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = [
  '/login',
  '/signin-redirect-oidc',
  '/signin-callback-oidc',
  '/signout-callback-out'
] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = getPageTitle(
    abp.localization.localize(
      to.meta.title,
      AppConsts.localization.defaultLocalizationSourceName
    )
  )

  // determine whether the user has logged in
  const hasToken = getToken()

  if (hasToken) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({ path: '/' })
      NProgress.done()
    } else {
      if (to.meta && to.meta.permission) {
        if (abp.auth.hasPermission(to.meta.permission)) {
          next()
        } else {
          next({ path: '/401' })
        }
      } else {
        next()
      }

      // const hasGetUserInfo = store.session.user
      // if (hasGetUserInfo) {
      //   // hack method to ensure that addRoutes is complete
      //   // set the replace: true, so the navigation will not leave a history record
      //   // next({ ...to, replace: true })
      //   if (abp.auth.hasPermission(to.meta.permission)) {
      //     next()
      //   } else {
      //     next({ path: '/401' })
      //   }
      // } else {
      //   // remove token and go to login page to re-login
      //   await store.dispatch('user/resetToken')
      //   Message.error('Has Error')
      //   next(`/login?redirect=${to.path}`)
      //   NProgress.done()
      // }
    }
  } else {
    /* has no token*/

    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
