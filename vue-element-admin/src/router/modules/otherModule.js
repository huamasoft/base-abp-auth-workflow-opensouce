﻿import Layout from '@/layout'

const otherRouter = {
  path: '/other',
  name: 'other',
  component: Layout,
  redirect: 'noRedirect',
  alwaysShow: true, // will always show the root menu
  hidden: false,
  meta: {
    title: 'Other',
    icon: 'example',
    permission: 'Pages.OtherModule'
  },
  children: [
    {
      path: 'commonTask',
      name: 'CommonTask',
      component: () => import('@/views/other/common-task/index'),
      meta: {
        title: 'CommonTask',
        permission: 'Pages.CommonTask'
      }
    },
    {
      path: 'advanceTask',
      name: 'AdvanceTask',
      component: () => import('@/views/other/advance-task/index'),
      meta: {
        title: 'AdvanceTask',
        permission: 'Pages.AdvanceTask'
      }
    },
    {
      path: 'simpleTask',
      name: 'SimpleTask',
      component: () => import('@/views/other/simple-task/index'),
      meta: {
        title: 'SimpleTask',
        permission: 'Pages.SimpleTask'
      }
    }
    // {#insert module code#}
  ]
}

export default otherRouter
