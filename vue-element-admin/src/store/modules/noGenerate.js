import { generateNoAsync } from '@/api/noGenerate'

const noGenerate = {
  namespaced: true,
  state: {
  },
  mutations: {
  },
  actions: {
    generateNoAsync({ state }, prefix) {
      return new Promise((resolve, reject) => {
        generateNoAsync(prefix)
          .then(response => {
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
      })
    }
  }
}

export default noGenerate
