import {
  getCurrentUserProfileForEdit,
  updateCurrentUserProfile,
  changeLanguage,
  changePassword,
  getProfilePicture,
  updateProfileRawPicture,
  getProfilePictureById
} from '@/api/profile'

const state = {
  avatar:
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAHlklEQVR4Xu1ba2hcRRQ+J9tHkibdzJk2aZu0P1QECwoKoiIoPkF8gIJS8YEUEcQiWpQqiH/EJ4g/BBGliPhGfKEIolK1IrUi1R9SEaxN27S17cy9TdI03U3ukbPsSpPcvTv3zqxS2gMhCzv3nDPfPefMmW9mEU5ywZN8/nAKgFMR0GYEmLkziqLLmPkiABhExAFm1oi4gpn7AWAaEf8GgH0AcAgA5PMuAPiOiDa32b32pYAx5hZEvAsArik6CWYeR8RPEHGjUmpTUT1ZzwWtAczcE8fxemZeBwBLAzv8JzM/rbXeGFJvEADqYf4gADwMACqkgym6diLi40qpN0LY8QYgiqK1SZI8ITkdwqEcOrYBwP1E9H2OZ+YMLQwAMy+w1r6DiDf5OOD57DQzb9BaP19UTyEAxsfHByqVyucAcF5Rw4Gfe5OI7iiiMzcAY2NjZ1UqlS8RcbCIwTY+861S6jpEHM9jIxcAURSdmyTJN4i4OI+R/3DsNqXUpYg45mrTGYA4jk9LkmQrAGhX5f/TuO+VUpcjYtXFvhMA1toyAPwMAKe7KM0akyQJHD16FCYnJ6FarUJHRwfMmzcPOjs7oaurCxCdXGrlxttEdFurQfJ9S2vMjNZayfkrXBQ2GyMTj6IIxsbGQD6niYChlILFixd7A8HMD7msDi0BiKLoMWZ+wnfy+/btg2PHjjmpkUhYtmyZNwiIeJ5SSvqFppIJwOjo6JJqtTqMiN1OnqcMkre9d+9eqFQquVR0d3fXQPCUH4jo4sIAGGM2IuJaHycOHjxYC/siIukgf55yAxF92kxH0wgYHR09c2pqajsAdBR1QN76nj17ij5eK5ArV66EUqlUWAcz/05EqxGR05Q0BcAY8yEi3ljYMgAYY+Dw4cM+KmDJkiW1ougptxPRW84ATExMDE5OThZ/dXVLu3fvri11PrJo0SIYGBjwUQHMvFVrfYEzANbaDQDwjJdVANi5c2fTJc9V94IFC2BoaMh1eNNxzLxKa7179oDUFLDWStPjvdHZsWOHt+Pz58+v1YEAsoGInmsJQKjwF0PDw8MwPT3t5bt0iCtWBKEaNhPRJS0BMMbcjYivenldf3j//v0wMTHhpUoKoBTCADKllCoj4gyH5qSAtfZ1ALgzgMHa+i99gI8sX768tkcIIR0dHVf19fV9dbyuOQAYY7YgYmrFzOsEM8OuXbsKp8HChQthcDAo7XAfEb3UCoAxROzJO9lm4yUFJBXyijRBkvuyCoQSZn5Ba72+KQB1dvdoKIMNPXlTQbbEsvbLfiCkMPNHWusZHOaMFGDm3iiKRkMaPR6EQ4cOSVOSqV7aXpm8VP/QwsxbtNZyQvWvzAAgiqI+Zo5CG27ok52htMZHjhyZszuUfO/p6YHe3t7aHqBN8hcRnZaVAl1RFPmtW46eCxjSJku4S7MTiAnKtM7MI1rrGW1l2jIonUvbXkHDQ2mQGpSYhL3Pjs8Rc0m/7Vrr1ZmrgLXWAAC5KnUdNzU1VesLhAsUZmg2LSZhL2kga76kgvCEbZA5BElaBPwKAOeEMi4TttbWJp5HBAjpACU9Asp7RLQmMwKMMR+EOO6SNyxdoBS8oiJ1gYigXBZS2l+E29RaP94qBZ4CgEd9zEl+CwmalwdsZlP6gf7+/hCrw21E9HYrAK4FgM+KAtCKBJWQllyXDk8+N5Y86RglWqRWpIn0BbIv8FktOjs7h7q7u0cyAWDm7iiKhMfKXYWkyREGOI3+lklLTsv/LBEgpGakRY/0CEuXFrt3kbYCiB+phIgx5gtEvDpvFIjjcRzPeawIu9uMTxSqvGCL/CwRPTLbuVQA5NIDM+e6iiJrunCAs0WKWF9fX14sa+PTQJCUWbVqVe560OyQJBUAuesTRZG8Smc+Oo3/D0FoSkrNXkK11nlXhh1ElHqumUWL5zoUmU2ASmcnXJ5vXy9FUSLr+E1UAZ7gXiJ6OS0MswAYQsQ/AcBpQz4yMjKj+AXi82s+z64tOSPrL6XUGYiYeiKbeTZojHkBER9wSWCpAQcOHKiBUKToZdmQty+kihyry+QFXNe9AzOv0Vq/10x/JgBxHKvp6Wm5luZ9NOMCYhvGbCOiTHrf5Xj8AaGS2uBc21WWSqULy+Xyj1mGWgJQz0HpDKVDPJHkUSJqebrlBIAxRlLgJ0Q88wRB4F0iutXFVycA6k2JnE8JCH4nlS5e+Y0Jf0mq4Y+19mwAkCvsYfanfhNNe/oXZr5Ua+1M7DpHQMOaXJyoVqtfI6L/kW1YADYppa5HxFwERG4AxOfx8fFl9auy54adQzFtzPya1rrQVZ5CAIibzLzQWvsGIt5czO0gT00h4nql1ItFtRUGoGHQGHMPAMh1efn5y38mzCx3GNZprbf4GPUGoB4NXXEcP5QkycOI2OvjUKtnmfkPAHhMa/1+q7Eu3wcBoGFIWuckSTYw871taJ9/A4Aniegdl4m5jgkKwPFGrbW3MvNaRLzS1ZmUcREzfwwAr/iGejMf2gZAwyAzL4rj+EpmPp+ZGz+b65eawcxC8Ekhk5/K7WXmg/XPw6VSaVO5XJbb6W2VtgPQVu8DKD8FQAAQT2gVJ30E/AMeEiVuLIiJPgAAAABJRU5ErkJggg==',
  uploadUrl: `${process.env.VUE_APP_BASE_API}/Profile/UploadProfilePicture`
}

const mutations = {
  SET_TOKEN: (state, token) => {
    // state.token = token
  }
}

const actions = {
  getCurrentUserProfileForEdit({ state }) {
    return new Promise((resolve, reject) => {
      getCurrentUserProfileForEdit()
        .then(response => {
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  updateCurrentUserProfile({ state }, payload) {
    return new Promise((resolve, reject) => {
      updateCurrentUserProfile(payload.data)
        .then(response => {
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  changeLanguage({ state }, payload) {
    return new Promise((resolve, reject) => {
      changeLanguage(payload.data)
        .then(() => {
          abp.utils.setCookieValue(
            'Abp.Localization.CultureName',
            payload.data.languageName,
            new Date(new Date().getTime() + 5 * 365 * 86400000),
            abp.appPath
          )
          window.location.reload()
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  changePassword({ state }, payload) {
    return new Promise((resolve, reject) => {
      changePassword(payload.data)
        .then(response => {
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  getProfilePicture({ state }) {
    return new Promise((resolve, reject) => {
      getProfilePicture()
        .then(response => {
          if (response.data.result.profilePicture.length) {
            state.avatar =
              'data:image/png;base64,' + response.data.result.profilePicture
          }
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  getProfilePictureById({ state, rootState }, profilePictureId) {
    return new Promise((resolve, reject) => {
      getProfilePictureById(profilePictureId)
        .then(response => {
          if (response.data.result.profilePicture.length) {
            rootState.user.avatar =
              'data:image/png;base64,' + response.data.result.profilePicture
          }
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  updateProfileRawPicture({ state, dispatch }, payload) {
    return new Promise((resolve, reject) => {
      updateProfileRawPicture(payload.data)
        .then(response => {
          dispatch('getProfilePicture')
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
