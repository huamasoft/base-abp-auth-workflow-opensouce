import crud from './common/crud'
import { getAllPermissions } from '@/api/role'

const role = {
  namespaced: true,
  state: {
    ...crud.state,
    permissions: []
  },
  mutations: {
    ...crud.mutations
  },
  actions: {
    ...crud.actions,
    getAllPermissions({ state }) {
      return new Promise((resolve, reject) => {
        state.loading = true
        getAllPermissions()
          .then(response => {
            state.permissions = []
            state.permissions.push(...response.data.result.items)
            resolve()
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            state.loading = false
          })
      })
    }
  }
}

export default role
