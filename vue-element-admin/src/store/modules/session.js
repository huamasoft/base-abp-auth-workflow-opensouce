import request from '@/utils/request'

const session = {
  namespaced: true,
  state: {
    application: null,
    user: null,
    tenant: null
  },
  actions: {
    async init({ state }) {
      // console.log(2, abp.multiTenancy.getTenantIdCookie())
      const rep = await request({
        url: '/api/services/app/Session/GetCurrentLoginInformations',
        method: 'get'
        // headers: {
        //   'Abp.TenantId': abp.multiTenancy.getTenantIdCookie()
        // }
      })
      rep.data.result.application.features = Object.assign(
        abp.features.allFeatures,
        rep.data.result.application.features,
      )
      // console.log(
      //   3,
      //   abp.multiTenancy.getTenantIdCookie(),
      //   rep.data.result.tenant
      // )

      state.application = rep.data.result.application
      state.user = rep.data.result.user
      state.tenant = rep.data.result.tenant
    },
    async getAbpUserConfiguration({ state }) {
      return await request({
        url: '/AbpUserConfiguration/GetAll',
        method: 'get'
      })
    }
  }
}
export default session
