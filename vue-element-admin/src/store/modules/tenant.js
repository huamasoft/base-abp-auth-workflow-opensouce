import crud from './common/crud'

const tenant = {
  namespaced: true,
  state: {
    ...crud.state
  },
  mutations: {
    ...crud.mutations
  },
  actions: {
    ...crud.actions
  }
}

export default tenant
