import axios from 'axios'
// import { MessageBox, Message } from 'element-ui'
// import store from '@/store'
// import { getToken } from '@/utils/auth'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  withCredentials: true, // send cookies when cross-domain requests
  timeout: 100000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    if (abp.auth.getToken()) {
      config.headers.common['Authorization'] = 'Bearer ' + abp.auth.getToken()
    }
    config.headers.common['.AspNetCore.Culture'] = abp.utils.getCookieValue(
      'Abp.Localization.CultureName'
    )
    config.headers.common['Abp.TenantId'] = abp.multiTenancy.getTenantIdCookie()
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    return response
  },
  error => {
    console.log(error) // for debug
    if (
      !!error.response &&
      !!error.response.data &&
      !!error.response.data.__abp
    ) {
      //   abp.ajax.showError(error.response.data.error)
      // } else {
      if (!error.response) {
        abp.ajax.showError(abp.ajax.defaultError)
        return
      }
      switch (error.response.status) {
        case 401:
          abp.ajax.handleUnAuthorizedRequest(
            abp.ajax.showError(abp.ajax.defaultError401),
            abp.appPath
          )
          break
        case 403:
          abp.ajax.showError(abp.ajax.defaultError403)
          break
        case 404:
          abp.ajax.showError(abp.ajax.defaultError404)
          break
        default:
          abp.ajax.showError(error.response.data.error)
          // abp.ajax.showError(error.response.data.error.details)
          // abp.ajax.showError(error.response.data.error.details)
          // abp.ajax.showError(abp.ajax.defaultError)
          break
      }
    }
    // if (error.config.url.endsWith('/api/TokenAuth/Authenticate')) {
    //   MessageBox.confirm(
    //     error.response.data.error.details,
    //     error.response.data.error.message,
    //     {
    //       confirmButtonText: '重新登录',
    //       showCancelButton: false,
    //       type: 'warning'
    //     }
    //   )
    // } else {
    //   if (error.response.data.error) {
    //     Message({
    //       message: error.response.data.error.message,
    //       type: 'error',
    //       duration: 5 * 1000
    //     })
    //   } else {
    //     Message({
    //       message: error.message,
    //       type: 'error',
    //       duration: 5 * 1000
    //     })
    //   }
    // }
    return Promise.reject(error)
  }
)

export default service
