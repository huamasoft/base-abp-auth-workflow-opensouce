import AppConsts from './appconst'
class SignalRAspNetCoreHelper {
  initSignalR() {
    var encryptedAuthToken = abp.utils.getCookieValue(
      AppConsts.authorization.encrptedAuthTokenName
    )

    abp.signalr = {
      autoConnect: true,
      connect: undefined,
      hubs: undefined,
      qs:
        AppConsts.authorization.encrptedAuthTokenName +
        '=' +
        encodeURIComponent(encryptedAuthToken),
      url: `${process.env.VUE_APP_BASE_API}/signalr`
    }

    // require('script-loader!abp-web-resources/Abp/Framework/scripts/libs/abp.signalr-client.js')
    require.ensure([], function(require) {
      require('script-loader!@aspnet/signalr/dist/browser/signalr.min.js')
      require('script-loader!abp-web-resources/Abp/Framework/scripts/libs/abp.signalr-client.js')
    })
    // jQuery.getScript(AppConsts.appBaseUrl + '/dist/abp/abp.signalr-client.js')
  }
}
export default new SignalRAspNetCoreHelper()
