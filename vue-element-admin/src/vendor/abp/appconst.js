const AppConsts = {
  userManagement: {
    defaultAdminUserName: 'admin'
  },
  localization: {
    defaultLocalizationSourceName: 'MyProject'
  },
  authorization: {
    encrptedAuthTokenName: 'enc_auth_token'
  },
  appBaseUrl: process.env.VUE_APP_BASE_API,
  remoteServiceBaseUrl: process.env.VUE_APP_BASE_API
}
export default AppConsts
